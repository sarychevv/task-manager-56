package ru.t1.sarychevv.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.project.ProjectClearRequest;

@Component
public class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECTS CLEAR]");
        getProjectEndpoint().clearProject(new ProjectClearRequest(getToken()));
    }

}
