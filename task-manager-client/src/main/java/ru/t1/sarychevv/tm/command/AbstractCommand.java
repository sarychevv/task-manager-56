package ru.t1.sarychevv.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.api.endpoint.ISystemEndpoint;
import ru.t1.sarychevv.tm.api.model.ICommand;
import ru.t1.sarychevv.tm.api.service.ITokenService;

@Getter
@Setter
@Component
public abstract class AbstractCommand implements ICommand {

    @NotNull
    @Autowired
    public ISystemEndpoint systemEndpoint;
    @NotNull
    @Autowired
    ITokenService tokenService;

    @Nullable
    protected String getToken() {
        return getTokenService().getToken();
    }

    protected void setToken(@Nullable final String token) {
        getTokenService().setToken(token);
    }

    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        String result = "";
        if (!name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (!description.isEmpty()) result += ": " + description;
        return result;
    }

}
