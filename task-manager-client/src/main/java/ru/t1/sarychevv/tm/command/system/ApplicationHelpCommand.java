package ru.t1.sarychevv.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.api.model.ICommand;
import ru.t1.sarychevv.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public class ApplicationHelpCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) System.out.println(command);
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-h";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show commands list.";
    }

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

}
