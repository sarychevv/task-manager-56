package ru.t1.sarychevv.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.user.UserLogoutRequest;

@Component
public class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "User logout.";
    }

    @NotNull
    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());
        getAuthEndpoint().logout(request);
    }

}
