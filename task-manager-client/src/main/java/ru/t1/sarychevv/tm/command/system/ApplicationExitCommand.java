package ru.t1.sarychevv.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.api.service.ILoggerService;

@Component
public class ApplicationExitCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        @NotNull final ILoggerService loggerService = getLoggerService();
        loggerService.info("** EXIT TASK MANAGER **");
        System.exit(0);
    }

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Close application";
    }

}
