package ru.t1.sarychevv.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.task.TaskChangeStatusByIndexRequest;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.util.TerminalUtil;

@Component
public class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Start task by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-start-by-index";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(getToken());
        request.setIndex(index);
        request.setStatus(Status.IN_PROGRESS);
        getTaskEndpoint().changeTaskStatusByIndex(request);
    }

}
