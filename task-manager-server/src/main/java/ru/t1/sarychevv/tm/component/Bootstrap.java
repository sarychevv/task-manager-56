package ru.t1.sarychevv.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.api.service.*;
import ru.t1.sarychevv.tm.api.service.dto.*;
import ru.t1.sarychevv.tm.dto.model.UserDTO;
import ru.t1.sarychevv.tm.endpoint.AbstractEndpoint;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class Bootstrap implements IServiceLocator {
    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;
    @Getter
    @NotNull
    @Autowired
    private IConnectionService connectionService;
    @Getter
    @NotNull
    @Autowired
    private IProjectDTOService projectDTOService;
    @Getter
    @NotNull
    @Autowired
    private ITaskDTOService taskDTOService;
    @Getter
    @NotNull
    @Autowired
    private IProjectTaskDTOService projectTaskDTOService;
    @Getter
    @NotNull
    @Autowired
    private IUserDTOService userDTOService;
    @NotNull
    @Getter
    @Autowired
    private ISessionDTOService sessionDTOService;
    @Getter
    @NotNull
    @Autowired
    private IAuthService authService;
    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    private void initEndpoints(@NotNull final AbstractEndpoint[] endpoints) {
        for (@NotNull AbstractEndpoint endpoint : endpoints) {
            registry(endpoint);
        }
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }


    @SneakyThrows
    public void start() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        loggerService.startJMSLogger();
        initPID();
        initEndpoints(endpoints);
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin("user");
        user.setPassword("user");
        user.setEmail("user@email.ru");
        @NotNull final UserDTO admin = new UserDTO();
        admin.setLogin("admin");
        admin.setPassword("admin");
        admin.setEmail("admin@email.ru");
        admin.setRole(Role.ADMIN);
        if (!userDTOService.isEmailExists(user.getEmail())) {
            userDTOService.add(user);
        }
        if (!userDTOService.isEmailExists(admin.getEmail())) {
            userDTOService.add(admin);
        }
    }

    public void stop() {
        loggerService.info("**TASK-MANAGER IS SHUTTING DOWN**");
    }


}
