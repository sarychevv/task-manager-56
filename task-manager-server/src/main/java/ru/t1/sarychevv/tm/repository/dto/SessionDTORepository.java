package ru.t1.sarychevv.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.sarychevv.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.sarychevv.tm.dto.model.SessionDTO;

@Repository
@Scope("prototype")
public final class SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO> implements ISessionDTORepository {

    private static final Class<SessionDTO> type = SessionDTO.class;

    public SessionDTORepository() {
        super(type);
    }

}

