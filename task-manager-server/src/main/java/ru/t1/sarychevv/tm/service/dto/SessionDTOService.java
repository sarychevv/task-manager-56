package ru.t1.sarychevv.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.sarychevv.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.sarychevv.tm.api.service.dto.ISessionDTOService;
import ru.t1.sarychevv.tm.dto.model.SessionDTO;
import ru.t1.sarychevv.tm.exception.entity.TaskNotFoundException;
import ru.t1.sarychevv.tm.exception.field.*;

import javax.persistence.EntityManager;

@Service
@NoArgsConstructor
public final class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO, ISessionDTORepository> implements ISessionDTOService {

    @NotNull
    protected ISessionDTORepository getRepository() {
        return context.getBean(ISessionDTORepository.class);
    }

    @NotNull
    @Override
    public SessionDTO updateById(@Nullable final String userId,
                                 @Nullable final String id,
                                 @Nullable final String name,
                                 @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final SessionDTO sessionDTO = findOneById(userId, id);
        if (sessionDTO == null) throw new TaskNotFoundException();
        sessionDTO.setName(name);
        sessionDTO.setDescription(description);
        @NotNull final ISessionDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(userId, sessionDTO);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return sessionDTO;
    }

    @NotNull
    @Override
    public SessionDTO updateByIndex(@Nullable final String userId,
                                    @Nullable final Integer index,
                                    @Nullable final String name,
                                    @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final SessionDTO sessionDTO = findOneByIndex(userId, index);
        if (sessionDTO == null) throw new TaskNotFoundException();
        sessionDTO.setName(name);
        sessionDTO.setDescription(description);
        @NotNull final ISessionDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(userId, sessionDTO);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return sessionDTO;
    }
}

