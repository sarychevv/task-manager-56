package ru.t1.sarychevv.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.sarychevv.tm.api.endpoint.IAuthEndpoint;
import ru.t1.sarychevv.tm.api.service.IAuthService;
import ru.t1.sarychevv.tm.api.service.dto.IUserDTOService;
import ru.t1.sarychevv.tm.dto.model.SessionDTO;
import ru.t1.sarychevv.tm.dto.model.UserDTO;
import ru.t1.sarychevv.tm.dto.request.user.UserLoginRequest;
import ru.t1.sarychevv.tm.dto.request.user.UserLogoutRequest;
import ru.t1.sarychevv.tm.dto.request.user.UserProfileRequest;
import ru.t1.sarychevv.tm.dto.response.user.UserLoginResponse;
import ru.t1.sarychevv.tm.dto.response.user.UserLogoutResponse;
import ru.t1.sarychevv.tm.dto.response.user.UserProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.sarychevv.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    @Autowired
    private IAuthService authService;

    @Autowired
    private IUserDTOService userDTOService;

    @Override
    @NotNull
    @WebMethod
    public UserLoginResponse login(@WebParam(name = REQUEST, partName = REQUEST)
                                   @NotNull final UserLoginRequest request) throws Exception {
        @NotNull String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @Override
    @NotNull
    @WebMethod
    public UserLogoutResponse logout(@WebParam(name = REQUEST, partName = REQUEST)
                                     @NotNull final UserLogoutRequest request) throws Exception {
        final SessionDTO session = check(request);
        authService.invalidate(session);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse profile(@WebParam(name = REQUEST, partName = REQUEST)
                                       @NotNull final UserProfileRequest request) throws Exception {
        @Nullable SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable UserDTO user = userDTOService.findOneById(userId);
        return new UserProfileResponse(user);
    }

}

